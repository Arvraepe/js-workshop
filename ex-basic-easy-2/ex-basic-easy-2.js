/* 

	Now that you know how to add files to your index.html file, we will take al ook at the building blocks of JavaScript.

	There are 3 big building blocks: functions and objects.

		function x () {  }

		var obj = { property: 'value' };

		var array = []; // can be mixed!

	Every building block is a value! This means that a function can also be stored in variables

		var x = function () {  }

	If a function can be a value, you can pass along a function to another function

		var x = function () {}
		var y = function (f) {}

		y(x);
	
	In this excercise we will create some functions. We'll start easy and work our way up. Try out different ways of defining your function if you want!

	1) create a function that prints out "Hello world"
	2) create a function that takes 1 parameter text and prints out the text variable
	3) create a function that takes 1 parameter and returns that parameter (identity function)
	4) create a function that takes 2 parameters and returns the sum of those two parameters (note that + also concattenate strings)

*/
