/*

	Now that we know how to create functions, let take a look at some other statements

	1) var-statement => declares a variable and is thus used for assignment. The statement is scoped to the nearest function definition
	2) if-statement => if (condition) {} else if (condition) {} else {} 
	3) for-loop => for (var i = 0; i < length; i++) {} (in JavaScript for loops can almost always be written as an array method (see functional excercises)) 
	4) while-loop => while (condition) {  } is almost never used except in very very specific cases

	We will now use the above statements when creating the next functions

	1) create a function that divides two parameters x and y (x / y) but make sure that you do not divide by 0
	2) create a function that prints out every integer number (1, 2, 3, ...) until the number that has passed along as a parameter
	3) create a function that prints out every integer number (1, 2, 3, ...) in reverse starting from the number passed along as a parameter, but use the while-loop

*/