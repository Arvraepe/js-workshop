/*
	Now that we have practiced our scoping, we can try and understand closures. This excercise isn't really something you need to do. It's something you need to GET.

	I told you about the primary building blocks of JavaScript: functions, objects, and arrays. Well, the most important one is a function! Why? Functions scope.

	Before we dive into ES6 (where functions and scopes are quite different thanks to the let & const statements) we will try and wrap our head around these functions scopes
	Try to understand the following code and output.

*/

var x = 1;

(function () {

	var y = 0;
	console.log('inside x ', x);
	console.log('inside y ', y);

}())

console.log('outside x ', x);
console.log('outside y ', y);

/*
	As you can see.. y is bound to the anonymous function that is executed immediately! This is a closure. It does nothing more than making sure that the y is not put on the
	global scope. If we take a look at the x variable. It's not bound to any function, this means it's available on the global scope... which is a bad thing! 
	Everything on the current page is able to interact with x. Most of the time this is not what you want!

	Ideally your application should only expose 1 variable to the global scope, that is your application namespace. 
*/

