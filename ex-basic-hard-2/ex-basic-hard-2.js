/*

	We will use our understanding of scopes to define modules. These modules will be contained scopes where we choose what is expose and what's not. 

	First of all we begin with a closure:

		(function () {
	
		}());

	But we will pass along some arguments

		(function (parent, undefined) {
	
		}(window));

	Parent will be the parent scope on which we will add the module. In this case we will expose to window (global scope) but we would also be able to provide another module
	to create a sub module. We only pass 1 parameter to the closure (window) so undefined will always have the value undefined. We do this because in older browsers you
	are able to give the variable undefined a value... and then your variable === undefined checks fail. In this case, they will not.

	After declaring the correct closure, we need to add the module to the parent. For the module itself, we will use a self-invoking function that returns an object which
	represents the public interface of the module.

		(function (parent, undefined) {
			
			parent['module'] = function () {
				return {
	
				};
			}();

		}(window));		

	This is the complete template of a module. Let's break it down. parent['module'] makes sure that your module gets added to the parent with the name (in this case) module. 
	It's equal to a function, that is executed directly. So the object it returns is stored in window.module.

	example

		(function (parent, undefined) {
			
			parent['module'] = function () {
				
				function add (x, y) { return x + y; }

				return {
					add: add
				};
			}();

		}(window));	


	For the next excercise, make a calculator module that has the window object as parent. The calculator module has add, substract, divide and multiply functions. 
	Create the module in this file and add it to an index.html to try it out!
*/

 