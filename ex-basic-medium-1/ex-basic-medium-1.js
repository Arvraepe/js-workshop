/*

	We know functions, objects, arrays, if statements, var statements, and loops. We have all the tools to create an application.
	This does not mean we know how to structure it properly. We should be able to understand scopes.

	Try to understand in this excercise what is being printed

	Mind that the WHOLE file is the excercise, so some lines may interact with other lines that happened before
*/

var x = 1;
var increment = function (n) { return n + 1; }
console.log(x);
console.log(increment(x));
console.log(n);

var countdown = function (x) { for (n = x; n >= 0; n--) { console.log(n); } };
countdown(5);
console.log(n);

function add (n, m) { return n + m }
add(n, 1);
countdown(10);

function a (b) {
	return function c (d) {
		return b + d;
	}
}

var x1 = a(1);
console.log(x1);
var y1 = x(2);
console.log(y1);
console.log(a(10)(20));

// Good luck! 