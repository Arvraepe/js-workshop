/*

	Now that we understand the basic building blocks (extended with the if and for statements), we will need to understand the way scoping works in JavaScript.
	JavaScript is a very dynamic environment to code in. But that also has a huge downside, if you have no clue what you are doing, you will make scoping mistakes 
	and almost unfindable bugs will creep into your growing application.

	For the next excercise you will need to predict the complete console output of the following code. When you think you have the solution, compare it with your own notes 

*/

var x = 10;
var y = 9;
var z = 8;

var countdown = function (n) { for (x = n; x >= 0; x--) { console.log(x === 0 ? 'liftoff' : x); } };

countdown(z);

var count = function (x) {
	return function (y) {
		return x + y;
	}
} 

console.log(x);
console.log(y);
console.log(z);

var x = count(2);
var y = x(3);
var z = count(1)(2);

console.log(x);
console.log(y);
console.log(z);

