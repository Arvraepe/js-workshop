/*

	First things first. Most ES6 features are available in the newer versions of chrome, but it's far from production ready.

	New stuff:
	let, const, fetch, promise, import, export, destruct, =>, ...

	First of all. Let & Const.

	var: scopes to nearest function
	let: scopes to nearest block or function
	const: scopes to nearest block or function but cannot be reassigned

	How to use these statements? 

	step 1) forget var
	step 2) try const
	step 3) if you need to reassign a const value, then change it to let

	Try to convert the following es5 code to the correct es6 usage
*/

function countdown (from) {
	var str = 'Counting ';
	for (var i = from; i >= 0; i++) {
		console.log(str+i+'...');
	}
}

countdown(10);

/*

	Disclaimer!
		
		const obj = { x: 1 };

	means that we cannot reassigne obj like this

		obj = 1;

	but we can still change its properties

		obj.x = 2;

	is therefore still valid!

*/