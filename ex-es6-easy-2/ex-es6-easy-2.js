/*

	Next up: arrow functions!

	JavaScript is function heavy and we use anonymous functions all the time. We can now use the => notation as syntactic sugar to transform

		[1,2,3].filter(function (n) { return n % 2 === 0; });

	to 

		[1,2,3].filter( (n) => n % 2 === 0 );

	You can still  define functions without parameters

	 	() => 2 + 2 or _ => 2 + 2

	Or create multiline functions

		() => {
			// do stuff
		}

	Try to convert the following ES5 code to ES6 using Arrow Functions

*/

var even = [1,2,3,4].filter(function (n) { return n % 2 === 0; });
var squared = [1,2,3,4].filter(function (n) { return n * n; });

function createObject (property, value) {
	return {
		property: value
	};
}

console.log(even);
console.log(squared);
console.log(createObject('x', 2));