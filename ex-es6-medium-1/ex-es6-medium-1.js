/*

	Two easy features down, a lot more to go!

	First of all we will be taking a look at destructing arrays and objects.

		const { name, age } = { name: 'Jos', male: true, hair: 'brown', age: 13, };
		console.log(name, age);

	We can easily get properties of an object and put them in variables

		const [first, ...rest] = [1,2,3,4,5,6]

		console.log('first', first);
		console.log('rest', rest);

	Or we can do the same with arrays

	This allows for easier recursion. Write a sum function that has the followin function signature

*/

function sum ([first, ...rest]) {

};

console.log(sum([1,2,3,4,5,6,7,8,9])); // = 1 + 2 + 3 + 4 + 5 ...