/*

	The next feature is import, export.

	ES6 allows you to require (import) modules in a JavaScript file. 

		import * from "module-name";
		import { member } from "module-name";

	This allows you to scope your JavaScript on file level. It's recommended to use the second notation (not the * wildcard)

	In another file (module-name), we can define the member as followed


		const member = { hello: function () { console.log('Hello!'); } };
		export { member: member };

	Excercise:

	Create a file called calculator-service.js and create a calculator with add, substract, multiply and divide exported
	Create a file that imports the calculator-service and uses these functions
*/