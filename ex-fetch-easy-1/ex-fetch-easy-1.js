/*

    The fetch API is part of the ES6 standard, but deserved a separate category.

    In this project you will find a Rest service called pokemon-server. This service contains some paths that you can explore.

    In the first exercise, you should use the fetch api to GET some information.

    1) Get the pokemon by name ivysaur.          /pokemon/by/name/:name
    2) Get a list of pokemon by type water.      /pokemon/all/by/type/:type

    You can run the Poke API by navigating (in the terminal) to the folder and type node server.js or npm start
 */
