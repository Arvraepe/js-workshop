/*

    Now that we have succesfully GET some information, it's time to POST some.

    The Pokemon API has a path for creating new trainers.
    To create a trainer, the API will need the following data:
    - name
    - age (should be greater or equal to 8)

    path /trainer/new

    Exercise Create yourself as a trainer in the Poke API

    You can run the Poke API by navigating (in the terminal) to the folder and type node server.js or npm start

 */
