/*

	Excercise Easy: Filtering arrays
	=============================================

	Filter the Pokemons array to the following arrays:

	1) A list of pokemons with the name starting with a certain text (for example ra => ratatta)
	2) A list of pokemons that have an attack greater than certain value (for example 50)
	3) A list of pokemons that have a certain type (for example, normal, grass, water, etc...)

	use ES6 if you want to! Arrow functions for the win!

*/

var filterName = function (partialName) { return Pokemons.filter(/* todo */); };
var filterAttack = function (attack) { return Pokemons.filter(/* todo */); };
var filterType = function (type) { return Pokemons.filter(/* todo */); };
