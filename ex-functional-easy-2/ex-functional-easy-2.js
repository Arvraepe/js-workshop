/*

	Excercise Easy: Every and Some
	=============================================

	Write validation functions with every and some. These functions return true or false. 

	1) Check if every name has a value
	2) Check if there is at least one female in the list

*/

var list = [{ name: 'Tom', male: true }, { name: 'Vanessa', male: false }, { name: 'Nick', male: true }];


