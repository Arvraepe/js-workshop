/*

	Excercise Easy 3: Reduce
	=======================================

	Use the reduce array function to transform a list of { id, name } objects to one object with { id: name }

	for example [{id: "cust_1", name: "Tom"}, {id: "cust_2", name: "Vanessa"}, {id: "cust_3", name: "Nick"}]
	will become { cust_1: "Tom", cust_2: "Vanessa", cust_3: "Nick" }

*/

var people = [{id: "cust_1", name: "Tom"}, {id: "cust_2", name: "Vanessa"}, {id: "cust_3", name: "Nick"}];
var peopleObj = /*  */;