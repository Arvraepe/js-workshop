/*

	Excercise Hard: Balanced scoring
	=======================================

	Imagine you writing an app that holds scores of teams. These teams can play different games (categories). 
	You want teams to compete and log their score of the game. These games are not competitive, but you want to show off the team with the best total score. 
	Imagine that you have two games one with an average score of 5 and another with the average score of 50. If we just count up the scores, 
	the team that scores best on the 50 average scored game will have an advantage, the weights of the scores are off balance.

	You are a clever programmer that says hey! I can fix this. The plan is that you add a multiplier to the total score of a category so the weights of the scores is balanced. 
	How will you do this? Calculate the average score for every game (category) the highest average score will be used as balance point (this will be multiplier 1) 
	The lower average scores will be compared to that balance point so for every category the multiplier will be HIGHEST_AVERAGE / average(category). 
	In case of the highest average score this multiplier will thus be 1.

	With this multiplier we will be able to calculate the real score of a game. For example game 1 has a multiplier of 2 (according to rules above). 
	If a team scores a total score of 5, it will have a real score of 10 ( 5 * multiplier, which is 2). These scores will be added for every game and we will have
	a balanced total score result.

	Steps:
	1) find all the averages for every game (category)
	2) select the highest average for that category this will be our balance point to calculate the multipliers
	3) calculate the multipliers for every game
	4) calculate the real score of a team for every category using the multiplier for that game
	5) sort that list to get the leaderboard with the real scoring

	Use the scores object below as input. 

*/

const scores = [
	{ team: 1, score: 5, game: { category: 1 } },
	{ team: 1, score: 6, game: { category: 1 } },
	{ team: 1, score: 7, game: { category: 1 } },
	{ team: 1, score: 3, game: { category: 1 } },
	{ team: 1, score: 2, game: { category: 1 } },
	{ team: 1, score: 3, game: { category: 1 } },
	{ team: 1, score: 4, game: { category: 1 } },
	{ team: 1, score: 6, game: { category: 1 } },
	{ team: 1, score: 7, game: { category: 1 } },
	{ team: 1, score: 9, game: { category: 1 } },
	{ team: 1, score: 4, game: { category: 1 } },
	{ team: 1, score: 5, game: { category: 1 } },
	{ team: 2, score: 4, game: { category: 1 } },
	{ team: 2, score: 8, game: { category: 1 } },
	{ team: 2, score: 8, game: { category: 1 } },
	{ team: 2, score: 5, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 1, game: { category: 1 } },
	{ team: 2, score: 2, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 4, game: { category: 1 } },
	{ team: 2, score: 6, game: { category: 1 } },
	{ team: 1, score: 50, game: { category: 2 } },
	{ team: 1, score: 65, game: { category: 2 } },
	{ team: 1, score: 72, game: { category: 2 } },
	{ team: 1, score: 33, game: { category: 2 } },
	{ team: 1, score: 28, game: { category: 2 } },
	{ team: 1, score: 36, game: { category: 2 } },
	{ team: 1, score: 47, game: { category: 2 } },
	{ team: 1, score: 62, game: { category: 2 } },
	{ team: 1, score: 73, game: { category: 2 } },
	{ team: 1, score: 90, game: { category: 2 } },
	{ team: 1, score: 45, game: { category: 2 } },
	{ team: 1, score: 55, game: { category: 2 } },
	{ team: 2, score: 43, game: { category: 2 } },
	{ team: 2, score: 82, game: { category: 2 } },
	{ team: 2, score: 80, game: { category: 2 } },
	{ team: 2, score: 51, game: { category: 2 } },
	{ team: 2, score: 38, game: { category: 2 } },
	{ team: 2, score: 19, game: { category: 2 } },
	{ team: 2, score: 29, game: { category: 2 } },
	{ team: 2, score: 39, game: { category: 2 } },
	{ team: 2, score: 39, game: { category: 2 } },
	{ team: 2, score: 38, game: { category: 2 } },
	{ team: 2, score: 45, game: { category: 2 } },
	{ team: 2, score: 62, game: { category: 2 } },
];