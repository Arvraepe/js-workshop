"use strict";
const scores = [
	{ team: 1, score: 5, game: { category: 1 } },
	{ team: 1, score: 6, game: { category: 1 } },
	{ team: 1, score: 7, game: { category: 1 } },
	{ team: 1, score: 3, game: { category: 1 } },
	{ team: 1, score: 2, game: { category: 1 } },
	{ team: 1, score: 3, game: { category: 1 } },
	{ team: 1, score: 4, game: { category: 1 } },
	{ team: 1, score: 6, game: { category: 1 } },
	{ team: 1, score: 7, game: { category: 1 } },
	{ team: 1, score: 9, game: { category: 1 } },
	{ team: 1, score: 4, game: { category: 1 } },
	{ team: 1, score: 5, game: { category: 1 } },
	{ team: 2, score: 4, game: { category: 1 } },
	{ team: 2, score: 8, game: { category: 1 } },
	{ team: 2, score: 8, game: { category: 1 } },
	{ team: 2, score: 5, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 1, game: { category: 1 } },
	{ team: 2, score: 2, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 3, game: { category: 1 } },
	{ team: 2, score: 4, game: { category: 1 } },
	{ team: 2, score: 6, game: { category: 1 } },
	{ team: 1, score: 50, game: { category: 2 } },
	{ team: 1, score: 65, game: { category: 2 } },
	{ team: 1, score: 72, game: { category: 2 } },
	{ team: 1, score: 33, game: { category: 2 } },
	{ team: 1, score: 28, game: { category: 2 } },
	{ team: 1, score: 36, game: { category: 2 } },
	{ team: 1, score: 47, game: { category: 2 } },
	{ team: 1, score: 62, game: { category: 2 } },
	{ team: 1, score: 73, game: { category: 2 } },
	{ team: 1, score: 90, game: { category: 2 } },
	{ team: 1, score: 45, game: { category: 2 } },
	{ team: 1, score: 55, game: { category: 2 } },
	{ team: 2, score: 43, game: { category: 2 } },
	{ team: 2, score: 82, game: { category: 2 } },
	{ team: 2, score: 80, game: { category: 2 } },
	{ team: 2, score: 51, game: { category: 2 } },
	{ team: 2, score: 38, game: { category: 2 } },
	{ team: 2, score: 19, game: { category: 2 } },
	{ team: 2, score: 29, game: { category: 2 } },
	{ team: 2, score: 39, game: { category: 2 } },
	{ team: 2, score: 39, game: { category: 2 } },
	{ team: 2, score: 38, game: { category: 2 } },
	{ team: 2, score: 45, game: { category: 2 } },
	{ team: 2, score: 62, game: { category: 2 } },
];

const filterCategory = (category) => scores.filter((score) => score.game.category === category);

const median = (category) => filterCategory(category).reduce((sum, score) => sum + score.score, 0) / filterCategory(category).length;

const getCategories = () => scores.reduce((arr, score) => {
	if (arr.indexOf(score.game.category) === -1) arr.push (score.game.category);
	return arr;
}, []);

const getHighestMedianScore = () => getCategories().map((category) => median(category)).sort((a, b) => b - a)[0];

const getMultiplierForCategory = (category) => getHighestMedianScore() / median(category); 

const getScoresForTeam = (team) => scores.filter((score) => score.team === team);

const getScoreOfCategoryForTeam = (team, category) => scores.filter((score) => score.team === team && score.game.category === category).reduce((sum, score) => sum+score.score, 0);

const getRealScoreForTeam = (team) => getCategories().map((category) => getScoreOfCategoryForTeam(team, category) * getMultiplierForCategory(category)).reduce((sum, score) => sum+score, 0);
 
console.log('1', median(1));
console.log('2', median(2));
console.log('highest', getHighestMedianScore());

console.log('1, 1', getScoreOfCategoryForTeam(1, 1));
console.log('1, 2', getScoreOfCategoryForTeam(1, 2));

console.log('2, 1', getScoreOfCategoryForTeam(2, 1));
console.log('2, 2', getScoreOfCategoryForTeam(2, 2));

console.log(getRealScoreForTeam(1));
console.log(getRealScoreForTeam(2));