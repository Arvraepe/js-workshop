/*

	Excercise Hard: Optional Monad
	=======================================

	The purpose of this excercise is to write a function that will encapsulate state and you will be able to modify it safely. 
	Monads are a way of passing along state through functions in a functional way, instead of defining it in a variable.

	The Optional monad will take a certain value and allows it to be modified without knowing if the values it transforms into will even be filled in.
	For example. When you have a user object with an optional field address, and that optional field address has an optional field street. When you want to 
	get the value of the street, you will have to use nested if-statements

	var street = 'default value';
	if (obj != null) { if (obj.address != null) { if (obj.address.street != null) { street = obj.address.street; } } }

	This is the imperative way of thinking.. now lets approach it functionally

	var street = Optional(obj).map(obj => obj.address).map(address => address.street).getOrElse('default value');

	Through the map functionality we can get values deeply nested in the object without knowing if there is an undefined value in between. We don't even care if that is the case
	we care if the value we want (in this case street) is available (getOrElse).

	Rules
	Optional().map(f) takes a function and returns another Optional() on which we can map, this optional may or may not be filled in.
	Optional().isEmpty() checks if the value is undefined or null
	Optional().getOrElse(d) takes a default value and returns the value if there is any in the optional.
*/

var Optional =  function ( value ) {

	return {

	}; 
};

var obj_with = { address: { street: 'test' } };
var obj_without = { address: null };

var optional_with = Optional(obj_with);
var optional_without = Optional(obj_without);

console.log('Should be filled: ', optional_with.map(obj => obj.address).map(address => address.street).getOrElse('impossible, should be filled!'));
console.log('Should not be filled: ', optional_without.map(obj => obj.address).map(address => address.street).getOrElse('notfilled'));