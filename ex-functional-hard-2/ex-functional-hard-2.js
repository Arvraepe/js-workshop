var Optional =  function ( value ) {
	var isEmpty = () => value === undefined || value === null;
	var map = (f) => isEmpty() ? Optional(null) : Optional(f(value));
	var getOrElse = (d) => isEmpty() ? d : value;
	return {
		isEmpty,
		map,
		getOrElse
	}; 
};

var obj_with = { address: { street: 'test' } };
var obj_without = { address: null };

var optional_with = Optional(obj_with);
var optional_without = Optional(obj_without);

console.log('Should be filled: ', optional_with.map(obj => obj.address).map(address => address.street).getOrElse(null));
console.log('Should not be filled: ', optional_without.map(obj => obj.address).map(address => address.street).getOrElse('notfilled'));