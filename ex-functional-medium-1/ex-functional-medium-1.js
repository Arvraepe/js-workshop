/*

	Exercise Medium: Advanced array methods
	=================================================

	Start from the products list and manage to transform it to the following lists

	1) list all products that are shipped but not been paid 
	2) list all products that are shipped and paid but only list the names
	3) Create a function that creates an object from the products array that contains the products 
		for example for customer 1 { "DELL Laptop": true, "Pokemon Yellow": false, "LHC Component CCHY": false } where true / false => if the product is paid or not
	4) Create a function that returns an object { customers: [1,2,3,4], total: 10, shipped: 5, paid: 5 }
*/

var Products = [
	{ id: 1, shipped: true, paid: true, customer: 1, name: "DELL Laptop" },
	{ id: 2, shipped: true, paid: true, customer: 2, name: "Raspberry PI" },
	{ id: 3, shipped: true, paid: false, customer: 1, name: "Pokemon Yellow" },
	{ id: 4, shipped: true, paid: false, customer: 2, name: "Call of Duty" },
	{ id: 5, shipped: true, paid: false, customer: 2, name: "Bureau Lundstrom" },
	{ id: 6, shipped: false, paid: true, customer: 3, name: "iPhone 6s" },
	{ id: 7, shipped: false, paid: true, customer: 3, name: "Samsung Monitor 24inch" },
	{ id: 8, shipped: false, paid: true, customer: 4, name: "Quantum Computer XCY1" },
	{ id: 9, shipped: false, paid: false, customer: 1, name: "LHC Component CCHY" },
	{ id: 10, shipped: false, paid: false, customer: null, name: "Headset Bose" }
];
