/*

    This track of exercises will learn you to create NodeJS applications and especially REST Webservices

    First exercise is to create a new project using the npm init functionality.

    Use this file (server.js) as entry point. When you have created your package.json you should be able to run
    this file with the command: npm start.

 */

console.log('You have successfully created your first node application');