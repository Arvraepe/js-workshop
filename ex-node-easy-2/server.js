/*

    In NodeJS your javascript file is your main entry point of the application. It does not matter if the
    application is a REST Service, or a CLI, this file is what will get executed.

    Let's try to load a file and display it's contents in the console!

    Try to use the fs module to load the file pokemons.json in the resources folder

    First part of the exercise
    ==============================
    1) import the fs module
    2) use that module to read the contents of /resources/pokemon.json
        A) Synchronously
        B) Asynchronously
    3) display the contents

    Second part of the exercise
    =============================
    Try to load the .json file as you would load a module (with the import statement)
    does this work?

    Third part of the exercise
    ============================
    1) Load the pokemon.json
    2) filter every pokemon that does not have the name ivysaur
    3) write that object to a file called ivysaur.json (with the fs) module
        A) Synchronously
        B) Asynchronously

 */