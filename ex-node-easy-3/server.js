/*

    You now know how to create a NodeJS Script. You can use this for anything, even for utilities... copying files,
    cron-jobs, CLI's, etc...

    But we will focus us on creating REST WebServices!

    We will be using the express framework to create a web service

    1) go ahead and install the express framework with npm (and save it to the dev dependencies of your package.json)
    2) import the express framework here in the server.js
    3) Start the server by listening to port 5001 (you can put a console.log when the service is started to check)

 */