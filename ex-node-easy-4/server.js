/*

    We have a web service but we are unable to do anything with it yet.

    Let's create a hello world path (GET). /hello

    that returns "Hello World!"

    1) install express with npm
    2) create and start the server listening to 5001
    (IF YOU HAVE EADDR ALREADY IN USE IT MEANS THAT YOUR PREVIOUS SERVER IS STILL RUNNING, killall node solves this.
    3) create the a GET path that will display hello world

    Second part
    ====================
    Let's create a path called /hello/:name that returns the name (path variable)

    extend your app (that already has the hello path)

    Third part
    =====================
    Let's add a query parameter to that path called lang. The default should be "en" and return
    Hello Name!

    Also support the following languages:
    nl => Hallo Name!
    fr => Bonjour Name!

    The URL should look like this: /hello/Arne?language=fr

    You can try these URL's in your favourite browser.
 */