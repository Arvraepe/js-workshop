/*

	Build chat application
	====================================

	Back-end functionality should be available at port 3000 when you are running the NodeJS application.
	The NodeJS should be located in the resources folder under the chat-server map.

	Use websocket to send strings to a server and add it to the list if it gets pushed back.

	use the "chat message" event to send (emit) a chat message to the server.
	use the "chat message" event to get (on) a chat message from the server.

*/