"use strict";

require('app-module-path').addPath(__dirname + '/src');

Array.prototype.first = function (otherwise) {	
	if (this.length === 0) return otherwise || null;
	else return this[0];
}; 

const Express = require('express');
const App = Express();
const BodyParser = require('body-parser');
const Cors = require('cors');

App.use(BodyParser.json());
App.use(Cors());

require('routes/PokemonRoutes').init(App);
require('routes/TrainerRoutes').init(App);

App.listen(9080, () => console.log('Started Poké API on port 9080') );