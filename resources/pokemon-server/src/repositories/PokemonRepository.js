const Pokemons = require('resources/pokemons.json');

exports.all = () => Pokemons;
exports.byId = (id) => Pokemons.filter((pokemon) => pokemon.national_id === id);
exports.byType = (type) => Pokemons.filter((pokemon) => pokemon.types.indexOf(type) !== -1);
exports.byName = (name) => Pokemons.filter((pokemon) => pokemon.name.toLowerCase() === name.toLowerCase());