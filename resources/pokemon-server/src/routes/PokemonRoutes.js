"use strict";

const PokemonRepository = require('repositories/PokemonRepository');

exports.init = (App) => {

	App.get('/pokemon/all', (req, res) => {
		res.send(PokemonRepository.all());
	});

	App.get('/pokemon/all/by/type/:type', (req, res) => {
		res.send(PokemonRepository.byType(req.params.type));
	});

	App.get('/pokemon/by/id/:id', (req, res) => {
		const id = parseInt(req.params.id);		
		const pokemon = PokemonRepository.byId(id).first();		
		if (pokemon) res.send(pokemon);
		else res.status(404).send({ error: 'NO_POKEMON_FOUND' });
	});	

	App.get('/pokemon/by/name/:name', (req, res) => {
		const pokemon = PokemonRepository.byName(req.params.name).first();
		if (pokemon) res.send(pokemon);
		else res.status(404).send({ error: 'NO_POKEMON_FOUND' });
	});

}