"use strict";

const TrainerValidator = require('validators/TrainerValidator');
const TrainerRepository = require('repositories/TrainerRepository');
const PokemonRepository = require('repositories/PokemonRepository');
const RandomHelper = require('helpers/RandomHelper');

exports.init = (App) => {
	
	App.post('/trainer/new', TrainerValidator.validateNewTrainer, (req, res) => {		
		res.send(TrainerRepository.create(req.body));
	});

}