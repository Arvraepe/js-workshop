exports.validateNewTrainer = (req, res, next) => {
	const errors = [];

	if (!req.body.name) errors.push('Trainer requires a name');
	if (!req.body.age) errors.push('Trainer requires an age');
	if (req.body.age < 8) errors.push('Trainer needs to be at least 8 years old');

	if (errors.length > 0) res.status(400).send({ errors: errors });
	else next();
};